//
//  ViewController.swift
//  SqlLite
//
//  Created by amr on 3/13/18.
//  Copyright © 2018 amr. All rights reserved.
//

import UIKit
import SQLite

class ViewController: UIViewController {
    var database: Connection!
    let usersTable = Table("users")
    let id = Expression<Int>("id")
    let name = Expression<String>("name")
    let email = Expression<String>("email")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        do {
             let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileUrl = documentDirectory.appendingPathComponent("users").appendingPathExtension("sqlite3")
            let database = try Connection(fileUrl.path)
            self.database = database
        }catch {print(error)}
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func createTableClicked(_ sender: Any) {
        print("create Tapped")
        
        let createTable = self.usersTable.create { (table) in
            table.column(self.id, primaryKey: true)
            table.column(self.name)
            table.column(self.email, unique: true)
            
        }
        do {
            try self.database.run(createTable)
            print("created table")
        } catch {
            print(error)
        }
    }
    @IBAction func insertUserClicked(_ sender: Any) {
        print("insert Tapped")
        let alert = UIAlertController(title: "Insert User", message: nil, preferredStyle: .alert)
        alert.addTextField { (tf) in tf.placeholder = "Name" }
        alert.addTextField { (tf) in tf.placeholder = "Email" }
        let action = UIAlertAction(title: "Submit", style: .default) { (_) in
            guard let name = alert.textFields?.first?.text,
            let emailString = alert.textFields?.last?.text
                else { return }
            print(name)
            print(emailString)
            let insertUser = self.usersTable.insert(self.name <- name, self.email <- emailString)
            do {
                try self.database.run(insertUser)
                print("inserted user")
            }catch { print(error) }
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    @IBAction func listUsersClicked(_ sender: Any) {
        print("list Tapped")
        do {
            let users = try self.database.prepare(self.usersTable)
            for user in users {
                print("userID : \(user[self.self.id]), name : \(user[self.name]), email : \(user[self.email])")
            }
        }catch { print(error)}
    }
    
    @IBAction func updateUserClicke(_ sender: Any) {
        print("update Tapped")
        let alert = UIAlertController(title: "Insert User", message: nil, preferredStyle: .alert)
        alert.addTextField { (tf) in tf.placeholder = "User ID" }
        alert.addTextField { (tf) in tf.placeholder = "Email" }
        let action = UIAlertAction(title: "Submit", style: .default) { (_) in
            guard let userIdString = alert.textFields?.first?.text,
                let userId = Int(userIdString),
                let emailString = alert.textFields?.last?.text
                else { return }
            print(userIdString)
            print(emailString)
            let user = self.usersTable.filter(self.id == userId)
            let updateUser = user.update(self.email <- emailString)
            do {
                try self.database.run(updateUser)
            }catch {print(error)}
    }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    @IBAction func deleteUserClicked(_ sender: Any) {
        print("update Tapped")
        let alert = UIAlertController(title: "Insert User", message: nil, preferredStyle: .alert)
        alert.addTextField { (tf) in tf.placeholder = "User ID" }
        
        let action = UIAlertAction(title: "Submit", style: .default) { (_) in
            guard let userIdString = alert.textFields?.first?.text,
                let userId = Int(userIdString)
            
                else { return }
            print(userIdString)
            
            let user = self.usersTable.filter(self.id == userId)
            let deleteUser = user.delete()
            do {
                try self.database.run(deleteUser)
            }catch {print(error)}
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
  
   
    
   
   
    
   
}

